import { gql } from "@apollo/client";

export const GET_PAGE = gql`
  query pages {
    page(id: 9, idType: DATABASE_ID ) {
      content(format: RENDERED)
      id
      pageId
      slug
      title(format: RENDERED)
      uri
    }
  }
`

export const GET_PAGE_BY_URI = gql`
	query GET_PAGE_BY_ID($id: ID!) {
	  page(id: $id, idType: URI) {
	    id
	    title
	    content(format: RENDERED)
	    slug
	    uri
	  }
	}
`;