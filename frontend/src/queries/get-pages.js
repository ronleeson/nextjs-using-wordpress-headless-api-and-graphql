import { gql } from "@apollo/client";

export const GET_PAGES = gql`
  {
    pages(where: {parent: "0"}) {
      edges {
        node {
          id
          pageId
          slug
          title(format: RENDERED)
          uri
        }
      }
    }
  }
`

export const GET_PAGES_URI = gql`
  {
    pages(where: {parent: "0"}) {
      edges {
        node {
          uri
          id
          children {
            nodes {
              uri
            }
          }
        }
      }
    }
  }
`

export const GET_ALL_PAGES = gql`
  {
    pages {
      edges {
        node {
          id
          pageId
          slug
          title(format: RENDERED)
          uri
        }
      }
    }
  }
`


