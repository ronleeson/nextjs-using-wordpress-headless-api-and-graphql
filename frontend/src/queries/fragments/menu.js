const MenuFragment = `
  fragment MenuFragment on MenuItem {
    id
    label
    uri
    path
    databaseId
  }
`

export default MenuFragment;