import { gql } from "@apollo/client";
import MenuFragment from "./fragments/menu";

export const Header = `
  
menuItems(where: {location: HCMS_MENU_HEADER, parentId: "0"}) {
    edges {
      node {
        ...MenuFragment
        childItems {
          edges {
            node {
              ...MenuFragment
            }
          }
        }
      }
    }
  }
  
`

export const GET_MENU_HEADER = gql`
query GET_MENU_HEADER {
  ${Header}
}
  ${MenuFragment}
`

export const clientMainMenuWithChildrenQuery = gql`
  query {
    menuItems(where: {location: HCMS_MENU_HEADER, parentId: "0"}) {
      edges {
        node {
          databaseId
          label
          uri
          title
          childItems {
            nodes {
              label
              uri
              title
              databaseId
              childItems {
                nodes {
                  label
                  title
                  uri
                  databaseId
                }
              }
            }
          }
        }
      }
    }
  }
`