import { ApolloProvider } from '@apollo/client';
import client from '../../src/apollo-client';
import '../../styles/globals.scss';
import Layout from '../components/layout'


export default function App({ Component, pageProps }) {
  return (
    <ApolloProvider client={client}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ApolloProvider>
  )
}