import client from '../../../src/apollo-client'
import { GET_PAGES_URI, GET_ALL_PAGES } from '../../../src/queries/get-pages';
import { GET_PAGE_BY_URI } from '../../../src/queries/get-page';

export default function Parent({ pageData }) {
  return (
    <div>
      <h1>Title: {pageData?.page?.title}</h1>
      
      <div
        dangerouslySetInnerHTML={{__html: pageData?.page?.content}}
      />
    </div>
  )
}

export const getStaticPaths = async () => {
  const { data, loading, networkStatus } = await client.query({
    query: GET_ALL_PAGES
  });

  const paths = data?.pages?.edges.map((slug) => {
    return {
      //paths: [{}, {}, { params: { slug: page.node.uri } }],
      //{ "foo": "bar", "pid": "abc" }
      //{ slug: [ 'about', 'my-about-child' ] }

      params: {
        // slug: slug.node.uri.toString()
        //
        //slug: [ 'about', 'my-about-child' ]
        slug: [slug.node.uri.toString()]
      }
    }
  })
  
  return {
    paths,
    fallback: true
  }
}

export const getStaticProps = async ({ params }) => {
  const { pageData } = params;
  const pageSlug = params.slug.join('/');
  const { data, loading, networkStatus } = await client.query({
    query: GET_PAGE_BY_URI,
    variables: {
      id: pageSlug
    }
  });
  return {
    props: {
      pageData: data
    }
  }
}