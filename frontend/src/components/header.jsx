import { useState, useEffect } from 'react';
import { usePathname } from 'next/navigation';
import Link from 'next/link';
import styles from '../../styles/Header.module.scss';
import { RxHamburgerMenu } from 'react-icons/rx';
import MobileMenu from './menus/mobileMenu';
import DesktopMenu from './menus/desktopMenu';

export default function Header() {
  const pathname = usePathname();
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  const [isDesktopMenuOpen, setIsDesktopMenuOpen] = useState(false);
  const [currentPath, setCurrentPath] = useState(pathname);

  const handleClientMobileMenu = () => {
    setIsMobileMenuOpen(!isMobileMenuOpen);
  };
  
  useEffect(() => {
    if (currentPath !== pathname) {
      setCurrentPath(pathname);
      setIsMobileMenuOpen(false);
      setIsDesktopMenuOpen(false);
    }
  }, [pathname]);




  return (
    <>
      <div className={styles.headerWrapperOuter}>
        <div className={styles.headerLeft}>
          <div className={styles.logoWrapper}>
            <h1><Link href="/">Logo</Link></h1>
          </div>
        </div>

        {}
        <div className={styles.headerRight}>
          <div onClick={handleClientMobileMenu}>
            <RxHamburgerMenu className={styles.hamburgerMenu} />
          </div>
        </div>
      </div>
      
      <div className={styles.desktopMenuWrapperOuter}>
      <DesktopMenu />
      </div>
      
      { isMobileMenuOpen ? <MobileMenu /> : null  }
      
    </>
  );
}