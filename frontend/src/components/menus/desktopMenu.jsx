
import client from '../../apollo-client';
import { useQuery, gql } from '@apollo/client';
import { GET_MENU_HEADER, clientMainMenuWithChildrenQuery } from '../../queries/get-menu';
// import the Header.module.scss file
import styles from '../../../styles/Header.module.scss';
import Link from 'next/link';

export default function DesktopMenu() {
  const { loading, error, data } = useQuery(clientMainMenuWithChildrenQuery);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error.message}</p>;

  const mainMenu = data.menuItems.edges.map((item) => {
    return (
      <li className={styles.topLevelLink} key={item.node.databaseId}>
        <Link href={item.node.uri}>{item.node.label}
        {item.node.childItems.nodes.length > 0 && <span className={styles.subMenuArrow}>&nbsp;&#x25BC;</span>}
        </Link>
        <ul className={styles.subMenu}>
          {item.node.childItems.nodes.map(childItem => {
            return (
              <li key={childItem.databaseId}><Link href={childItem.uri}>{childItem.label} 
                {childItem.childItems.nodes.length > 0 && <span className={styles.subMenuArrow}>&nbsp;&#x25B6;</span>}
              </Link>
                <ul>
                  {childItem.childItems.nodes.map(grandChildItem => {
                    return (
                      <li key={grandChildItem.databaseId}><Link href={grandChildItem.uri}>{grandChildItem.label}</Link></li>
                    )
                  })}
                </ul>
              </li>
            )
          })}
        </ul>
      </li>
    )
  })

  console.log(data);
  return (
    <div className={styles.headerWrapper}>
      <div className={styles.desktopMenuWrapperOuter}>
        <ul className={styles.menuWrapper}>
          {mainMenu}
        </ul>
      </div>
    </div>
  )
}