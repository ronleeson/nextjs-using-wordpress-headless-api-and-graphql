<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'Attribute' => $vendorDir . '/symfony/polyfill-php80/Resources/stubs/Attribute.php',
    'Composer\\InstalledVersions' => $vendorDir . '/composer/InstalledVersions.php',
    'GraphQLRelay\\Connection\\ArrayConnection' => $vendorDir . '/ivome/graphql-relay-php/src/Connection/ArrayConnection.php',
    'GraphQLRelay\\Connection\\Connection' => $vendorDir . '/ivome/graphql-relay-php/src/Connection/Connection.php',
    'GraphQLRelay\\Mutation\\Mutation' => $vendorDir . '/ivome/graphql-relay-php/src/Mutation/Mutation.php',
    'GraphQLRelay\\Node\\Node' => $vendorDir . '/ivome/graphql-relay-php/src/Node/Node.php',
    'GraphQLRelay\\Node\\Plural' => $vendorDir . '/ivome/graphql-relay-php/src/Node/Plural.php',
    'GraphQLRelay\\Relay' => $vendorDir . '/ivome/graphql-relay-php/src/Relay.php',
    'PhpToken' => $vendorDir . '/symfony/polyfill-php80/Resources/stubs/PhpToken.php',
    'Stringable' => $vendorDir . '/symfony/polyfill-php80/Resources/stubs/Stringable.php',
    'UnhandledMatchError' => $vendorDir . '/symfony/polyfill-php80/Resources/stubs/UnhandledMatchError.php',
    'ValueError' => $vendorDir . '/symfony/polyfill-php80/Resources/stubs/ValueError.php',
    'WPGraphQLGutenberg\\Admin\\Editor' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Admin/Editor.php',
    'WPGraphQLGutenberg\\Admin\\Settings' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Admin/Settings.php',
    'WPGraphQLGutenberg\\Blocks\\Block' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Blocks/Block.php',
    'WPGraphQLGutenberg\\Blocks\\Registry' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Blocks/Registry.php',
    'WPGraphQLGutenberg\\Blocks\\RegistryNotSourcedException' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Blocks/Registry.php',
    'WPGraphQLGutenberg\\Blocks\\Utils' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Blocks/Utils.php',
    'WPGraphQLGutenberg\\PostTypes\\BlockEditorPreview' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/PostTypes/BlockEditorPreview.php',
    'WPGraphQLGutenberg\\PostTypes\\ReusableBlock' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/PostTypes/ReusableBlock.php',
    'WPGraphQLGutenberg\\Rest\\Rest' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Rest/Rest.php',
    'WPGraphQLGutenberg\\Schema\\Schema' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Schema/Schema.php',
    'WPGraphQLGutenberg\\Schema\\Types\\BlockTypes' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Schema/Types/BlockTypes.php',
    'WPGraphQLGutenberg\\Schema\\Types\\Connection\\BlockEditorContentNodeConnection' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Schema/Types/Connection/BlockEditorContentNodeConnection.php',
    'WPGraphQLGutenberg\\Schema\\Types\\InterfaceType\\Block' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Schema/Types/InterfaceType/Block.php',
    'WPGraphQLGutenberg\\Schema\\Types\\InterfaceType\\BlockEditorContentNode' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Schema/Types/InterfaceType/BlockEditorContentNode.php',
    'WPGraphQLGutenberg\\Schema\\Types\\Object\\ReusableBlock' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Schema/Types/Object/ReusableBlock.php',
    'WPGraphQLGutenberg\\Schema\\Types\\Scalar\\Scalar' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Schema/Types/Scalar/Scalar.php',
    'WPGraphQLGutenberg\\Schema\\Utils' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Schema/Utils.php',
    'WPGraphQLGutenberg\\Server\\Server' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Server/Server.php',
    'WPGraphQLGutenberg\\Server\\ServerException' => $baseDir . '/wp-content/plugins/wp-graphql-gutenberg/src/Server/Server.php',
    'WPGraphQL\\JWT_Authentication\\Auth' => $baseDir . '/wp-content/plugins/wp-graphql-jwt-authentication/src/Auth.php',
    'WPGraphQL\\JWT_Authentication\\Login' => $baseDir . '/wp-content/plugins/wp-graphql-jwt-authentication/src/Login.php',
    'WPGraphQL\\JWT_Authentication\\ManageTokens' => $baseDir . '/wp-content/plugins/wp-graphql-jwt-authentication/src/ManageTokens.php',
    'WPGraphQL\\JWT_Authentication\\RefreshToken' => $baseDir . '/wp-content/plugins/wp-graphql-jwt-authentication/src/RefreshToken.php',
);
